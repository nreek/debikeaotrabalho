<?php 
add_action( 'wp_enqueue_scripts', 'salient_child_enqueue_styles');
function salient_child_enqueue_styles() {
	
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css', array('font-awesome'));
	
	if ( is_rtl() ) 
	wp_enqueue_style(  'salient-rtl',  get_template_directory_uri(). '/rtl.css', array(), '1', 'screen' );
}

function delete_post_type(){
	unregister_post_type( 'portfolio' );
}
add_action('init','delete_post_type');

// Register Custom Post Type
function materiais() {
	
	$labels = array(
		'name'                  => _x( 'Materiais', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Material', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Materiais', 'text_domain' ),
		'name_admin_bar'        => __( 'Materiais', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Material', 'text_domain' ),
		'description'           => __( 'Materiais para download', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'materiais', $args );
	
}
add_action( 'init', 'materiais', 0 );

function register_metaboxes( $meta_boxes ) {
	$prefix = 'materiais';
	
	$meta_boxes[] = array(
		'id' => 'register_metaboxes',
		'title' => esc_html__( 'Informações', 'Informações' ),
		'post_types' => array( 'materiais', 'portfolio', 'posts'),
		'context' => 'advanced',
		'priority' => 'default',
		'autosave' => false,
		'fields' => array(
			array(
				'id' => $prefix . 'meteriais_download-link',
				'type' => 'text',
				'name' => esc_html__( 'Link de Download do Material', 'Link de Download do Material' ),
			),
		),
	);
	
	$meta_boxes[] = array(
		'id' => 'markers_metaboxes',
		'title' => esc_html__( 'Informações de Marcadores', 'Informações de Marcadores' ),
		'post_types' => array( 'markers'),
		'context' => 'advanced',
		'priority' => 'top',
		'autosave' => false,
		'fields' => array(
			array(
				'id' => 'marker_latitude',
				'type' => 'text',
				'name' => esc_html__( 'Latitude', 'Latitude' ),
			),
			array(
				'id' => 'marker_longitude',
				'type' => 'text',
				'name' => esc_html__( 'Longitude', 'Longitude' ),
			),
		),
	);
	
	
	return $meta_boxes;
}
add_filter( 'rwmb_meta_boxes', 'register_metaboxes' );

function get_dbat_maps($atts) {
	ob_start();
	include 'map.php';
	$buffer = ob_get_contents();
	@ob_end_clean();
	return $buffer;
}
add_shortcode('dbat_maps', 'get_dbat_maps');

function get_dbat_geoloc_form($atts) {
	ob_start();
	include 'geolocation_form.php';
	$buffer = ob_get_contents();
	@ob_end_clean();
	return $buffer;
}
add_shortcode('dbat_geolocation_form', 'get_dbat_geoloc_form');

// Register Custom Post Type
function markers_post_type() {
	
	$labels = array(
		'name'                  => _x( 'Marcadores', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Marcador', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Marcadores', 'text_domain' ),
		'name_admin_bar'        => __( 'Marcador', 'text_domain' ),
		'archives'              => __( 'Item Archives', 'text_domain' ),
		'attributes'            => __( 'Item Attributes', 'text_domain' ),
		'parent_item_colon'     => __( 'Parent Item:', 'text_domain' ),
		'all_items'             => __( 'All Items', 'text_domain' ),
		'add_new_item'          => __( 'Add New Item', 'text_domain' ),
		'add_new'               => __( 'Add New', 'text_domain' ),
		'new_item'              => __( 'New Item', 'text_domain' ),
		'edit_item'             => __( 'Edit Item', 'text_domain' ),
		'update_item'           => __( 'Update Item', 'text_domain' ),
		'view_item'             => __( 'View Item', 'text_domain' ),
		'view_items'            => __( 'View Items', 'text_domain' ),
		'search_items'          => __( 'Search Item', 'text_domain' ),
		'not_found'             => __( 'Not found', 'text_domain' ),
		'not_found_in_trash'    => __( 'Not found in Trash', 'text_domain' ),
		'featured_image'        => __( 'Featured Image', 'text_domain' ),
		'set_featured_image'    => __( 'Set featured image', 'text_domain' ),
		'remove_featured_image' => __( 'Remove featured image', 'text_domain' ),
		'use_featured_image'    => __( 'Use as featured image', 'text_domain' ),
		'insert_into_item'      => __( 'Insert into item', 'text_domain' ),
		'uploaded_to_this_item' => __( 'Uploaded to this item', 'text_domain' ),
		'items_list'            => __( 'Items list', 'text_domain' ),
		'items_list_navigation' => __( 'Items list navigation', 'text_domain' ),
		'filter_items_list'     => __( 'Filter items list', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Marcador', 'text_domain' ),
		'description'           => __( 'Atividades - Marcadores', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'custom-fields' ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'page',
	);
	register_post_type( 'markers', $args );
	
}
add_action( 'init', 'markers_post_type', 0 );

add_action( 'wp_enqueue_scripts', 'secure_enqueue_script' );
function secure_enqueue_script() {
	wp_register_script( 'secure-ajax-access', esc_url( add_query_arg( array( 'js_global' => 1 ), site_url() ) ) );
	wp_enqueue_script( 'secure-ajax-access' );
}

//Joga o nonce e a url para as requisições para dentro do Javascript criado acima
add_action( 'template_redirect', 'javascript_variaveis' );
function javascript_variaveis() {
	if ( !isset( $_GET[ 'js_global' ] ) ) return;
	
	$nonce = wp_create_nonce('mais_noticias_nonce');
	
	$variaveis_javascript = array(
		'mais_noticias_nonce' => $nonce, //Esta função cria um nonce para nossa requisição para buscar mais notícias, por exemplo.
		'xhr_url'             => admin_url('admin-ajax.php') // Forma para pegar a url para as consultas dinamicamente.
	);
	
	$new_array = array();
	foreach( $variaveis_javascript as $var => $value ) $new_array[] = esc_js( $var ) . " : '" . esc_js( $value ) . "'";
	
	header("Content-type: application/x-javascript");
	printf('var %s = {%s};', 'js_global', implode( ',', $new_array ) );
	exit;
}

add_action('wp_ajax_nopriv_mais_noticias', 'mais_noticias');
add_action('wp_ajax_mais_noticias', 'mais_noticias');

function mais_noticias() {
	$upload_dir = wp_upload_dir();
	$upload_url = $upload_dir['baseurl'] ?: site_url() . '/wp-content/uploads';
	$pin_url = $upload_url . "/2018/03/pin-". (getRequestInfo($_REQUEST['atividade'], 'fpessoa') == 'Empresa' ? 'verde' : 'azul' ) .".png";
	
	$content = "
	<p>".getRequestInfo($_REQUEST['atividade'], 'ftipoatividade')."</p>
	<p><b>QUEM ESTÁ PROPONDO:</b> ".getRequestInfo($_REQUEST['atividade'], 'fname')."</p>
	<p><span><img src='$pin_url' style='width:15px; float:left'></span> &nbsp;".getRequestInfo($_REQUEST['atividade'], 'flocal')."</p><p>".getRequestInfo($_REQUEST['atividade'], 'fdescricao')."</p>
	<a href='".getRequestInfo($_REQUEST['atividade'], 'linkiniciativa')."' target='_blank'>Saiba Mais</a>
	<br>
	";
	
	if(getRequestInfo($_REQUEST['atividade'], 'fpessoa') == 'Empresa'){
		$content .="<a class='icon' href='".getRequestInfo($_REQUEST['atividade'], 'fsite')."'>".getRequestInfo($_REQUEST['atividade'], 'fsite')."</a>";
		
		if(getRequestInfo($_REQUEST['atividade'], 'ffacebook') && getRequestInfo($_REQUEST['atividade'], 'ffacebook') != '')
			$content .= "<a class='icon' href='".getRequestInfo($_REQUEST['atividade'], 'ffacebook')."'><i class='fa fa-facebook'></i></a>";
			
		if(getRequestInfo($_REQUEST['atividade'], 'ftwitter') && getRequestInfo($_REQUEST['atividade'], 'ftwitter') != '')
		$content .= "<a class='icon' href='".getRequestInfo($_REQUEST['atividade'], 'ftwitter')."'><i class='fa fa-twitter'></i></a> ";
		
		if(getRequestInfo($_REQUEST['atividade'], 'finstagram') && getRequestInfo($_REQUEST['atividade'], 'finstagram') != '')
		$content .= "<a class='icon' href='".getRequestInfo($_REQUEST['atividade'], 'finstagram')."'><i class='fa fa-instagram'></i></a> ";

		if(getRequestInfo($_REQUEST['atividade'], 'linkedin') && getRequestInfo($_REQUEST['atividade'], 'linkedin') != '')
		$content .= "<a class='icon' href='".getRequestInfo($_REQUEST['atividade'], 'linkedin')."'><i class='fa fa-linkedin'></i></a>";
	}
	
	$post_id = wp_insert_post([
		'post_type' => 'markers',
		'post_title' => getRequestInfo($_REQUEST['atividade'], 'fatividade'),
		'post_content' => $content,
		'post_status'=> 'draft',
		]);
		
		add_post_meta( $post_id, 'cor', getRequestInfo($_REQUEST['atividade'], 'fpessoa') == 'Empresa' ? 'verde' : 'azul' );
		add_post_meta( $post_id, 'latitude', $_REQUEST['place']['latitude']);
		add_post_meta( $post_id, 'longitude', $_REQUEST['place']['longitude']);
		add_post_meta( $post_id, 'sugestoes', getRequestInfo($_REQUEST['atividade'], 'fdicassugestoes'));
		add_post_meta( $post_id, 'tipo_atividade', getRequestInfo($_REQUEST['atividade'], 'fatividade')  );
		add_post_meta( $post_id, 'divulgacao', getRequestInfo($_REQUEST['atividade'], 'divulgatividade')  );
		add_post_meta( $post_id, 'tipo_pessoa', getRequestInfo($_REQUEST['atividade'], 'atividade')  );
		add_post_meta( $post_id, 'email', getRequestInfo($_REQUEST['atividade'], 'femail')  );
		
		if(getRequestInfo($_REQUEST['atividade'], 'fpessoa') == 'Indivíduo'){
			add_post_meta( $post_id, 'distancia_casa_trabalho', getRequestInfo($_REQUEST['atividade'], 'fdistancia')  );
			add_post_meta( $post_id, 'meio_transporte', getRequestInfo($_REQUEST['atividade'], 'meiotransporte')  );
		}
		
		wp_die();
	}
	
	function getRequestInfo($ar, $var){
		foreach($ar as $v){
			if($v['name'] == $var){
				return $v['value'];
			}
		}
	}
	
	function get_map_stats($atts) {
		$atts = shortcode_atts(
			array(
				'dia_evento' => date('d/m/Y'),
			), $atts, 'dbat_map_stats' );
			
			ob_start();
			include 'map_stats.php';
			$buffer = ob_get_contents();
			@ob_end_clean();
			return $buffer;
		}
		add_shortcode('dbat_map_stats', 'get_map_stats');