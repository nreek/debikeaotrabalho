<?php
$query = new WP_Query([
    'post_type' => ['markers'],
    'posts_per_page' => -1
    ]);

$upload_dir = wp_upload_dir();
$upload_url = $upload_dir['baseurl'] ?: site_url() . '/wp-content/uploads';

?>

    <style>
    #map {
        height: 100%;
        min-height: 500px;
    }

    .map-caption{
        background: white;
        box-shadow: 0 0 15px rgba(0,0,0,0.3);
        padding: 10px;
        position: absolute;
        top: 130px;
        left: 15px;
        z-index: 10;
        display: flex;
    }

    .map-caption > div{
        display: flex;
        align-items: center;
        margin-right: 15px;
    }

    .map-caption img{
        width: 25px !important;
        margin: 0px !important;
    }

    .map-caption .azul{
        color: #3372b9;
    }

    .map-caption .verde{
        color: #47cfc4;
    }
    </style>
    <div class="map-caption">
        <div class="verde"><img src="<?= $upload_url; ?>/2018/03/pin-verde.png" alt="">Atividade de Empresas</div>
        <div class="azul"><img  src="<?= $upload_url; ?>/2018/03/pin-azul.png" alt="">Atividade de Ciclistas</div>
    </div>
    <div id="map"></div>
    <script>
    'use strict';

    function initMap() {
        // var uluru = {lat: -25.363, lng: 131.044};

        var styledMapType = new google.maps.StyledMapType([
    {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "color": "#444444"
            }
        ]
    },
    {
        "featureType": "administrative.province",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "all",
        "stylers": [
            {
                "color": "#f2f2f2"
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road",
        "elementType": "all",
        "stylers": [
            {
                "saturation": -100
            },
            {
                "lightness": 45
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "all",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "water",
        "elementType": "all",
        "stylers": [
            {
                "color": "#46bcec"
            },
            {
                "visibility": "on"
            }
        ]
    }
]);

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 5,
            center: {lat: -23.5505199, lng: -46.6333094},
			disableDefaultUI: true,
            zoomControl: true,
        });

        map.mapTypes.set('styled_map', styledMapType);
        map.setMapTypeId('styled_map');
        
        <?php 
        while($query->have_posts()){
            $query->the_post();
            $latitude = get_post_meta(get_the_ID(), 'latitude');
            $longitude = get_post_meta(get_the_ID(), 'longitude');
            $cor = get_post_meta(get_the_ID(), 'cor');

            if($latitude != ''&& $longitude != ''){
                ?>
                var contentString<?= get_the_ID() ?> = `<div id="content">
                <div id="siteNotice<?= get_the_ID() ?>">
                </div>
                <h1 id="firstHeading" class="firstHeading"><?php the_title() ?></h1>
                <div id="bodyContent"><?php the_content() ?></div>
                </div>`;
                
                var infowindow<?= get_the_ID() ?> = new google.maps.InfoWindow({
                    content: contentString<?= get_the_ID() ?>
                });
                
                var icon<?= get_the_ID() ?> = {
                    url: '<?= $upload_url; ?>/2018/03/pin-<?= $cor[0] ?>.png', // url
                    scaledSize: new google.maps.Size(40, 40), // scaled size
                    origin: new google.maps.Point(0,0), // origin
                    anchor: new google.maps.Point(0, 0) // anchor
                };

                var marker<?= get_the_ID() ?> = new google.maps.Marker({
                    position: {lat: <?= $latitude[0] ?>, lng: <?= $longitude[0] ?>},
                    map: map,
                    icon : icon<?= get_the_ID() ?>,
                    title: '<?php the_title() ?>'
                });
                marker<?= get_the_ID() ?>.addListener('click', function() {
                    infowindow<?= get_the_ID() ?>.open(map, marker<?= get_the_ID() ?>);
                });
                <?php
            }
        }
        ?>
        
    }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDD6lYTKhVN9Q_S6mE0jKTdCdzuYp1mROA&callback=initMap">
    </script>
