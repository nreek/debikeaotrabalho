<?php
$query = new WP_Query([
    'post_type' => ['markers'],
    'posts_per_page' => -1
    ]);
    
    $qtdMarkers = 0;
    $qtdMarkers = 0;
    $qtdEmpresas = 0;
    
    while($query->have_posts()){
        $query->the_post();
        $qtdMarkers++;
        
        $qtdKilometers += (int)get_post_meta(get_the_ID(), 'distancia_casa_trabalho')[0];
        
        if(get_post_meta(get_the_ID(), 'tipo_pessoa')[0] == "Empresa")
        $qtdEmpresas++;
        
    }
    
    ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>

    <script>
    setTimeout(function(){
        jQuery('#stats_days').html((moment('<?= $atts['dia_evento'] ?>', 'DD/MM/YYYY').diff(moment(), 'days')));
    }, 1000);
    </script>
<div class="row-flex map-stats">
    <div class="col-md-4">
        <span class="gray">FALTAM</span>
        <h3 id="stats_days">0</h3>
        <span class="black">dias para o dia de bike ao trabalho</span>
    </div>
    <div class="col-md-4">
        <span class="gray">TEMOS</span>
        <h3>
            <?= $qtdMarkers ?>
        </h3>
        <span class="black">atividades cadastradas</span>
    </div>

    <div class="col-md-4">
        <span class="gray">TEMOS</span>
        <h3>
            <?= $qtdEmpresas ?>
        </h3>
        <span class="black">empresas comprometidas</span>
    </div>

    <!-- <div class="col-md-4">
    <h3><?= (int)$qtdKilometers ?> kilômetros percorridos </h3>
    </div> -->
</div>
    <style>
    
    .map-stats .col-md-4{
        text-align: center;
        font-family : Roboto Condensed;
    }
    
    .map-stats .gray{
        font-weight: 500;
        color: #7b7b7b;
        font-size: 20px
    }
    
    .map-stats .col-md-4 h3{
        color: #6ba1d0;
        font-size: 100px !important;
        font-weight: 900 !important;
        font-family: Roboto Condensed !important;
        text-align: center;
        line-height: 110px !important;
        border-bottom: 1px solid black;
        max-width: 160px;
        margin: 0 auto 15px;
    }
    
    .map-stats .col-md-4 .black{
        text-transform: uppercase;
        font-size: 23px;
        font-weight: 100;
        max-width: 240px;
        display: block;
        margin:  0 auto;
    }
    
    
    .row-flex{
        display: flex;
        flex-wrap: wrap;
        max-width: 1300px;
        margin: 0 auto;
    }
    
    .row-flex h3{
        text-transform: none !important;
        font-size: 30px !important;
        font-weight: 500 !important;
        text-align: center;
    }
    
    .row-flex .col-md-4{
        max-width: 33.3%;
        flex: 0 0 33.3%
    }
    
    @media screen and (max-width: 700px){
        .row-flex .col-md-4{
            max-width: 100%;
            flex: 0 0 100%
        }   
    }
    </style>