<script>
var placeSearch, autocomplete;
var placeObj = { 'latitude' : '',  'longitude' : '' };
    
    setTimeout(function(){
        jQuery('#autocomplete').focus(function(){
            geolocate();
        });
    }, 500);
    
    function initAutocomplete() {
        autocomplete = new google.maps.places.Autocomplete( (document.querySelector('[name=flocal]')), {types: ['geocode']});
        
        autocomplete.addListener('place_changed', fillInAddress);
    }
    
    function fillInAddress() {
        var place = autocomplete.getPlace();
        
        for (var i = 0; i < place.address_components.length; i++) {
            var addressType = place.address_components[i].types[0];
            var val = place.address_components[i]['long_name'];
            placeObj[addressType] = val;
        }		
        placeObj['latitude'] = place.geometry.location.lat();
        placeObj['longitude'] = place.geometry.location.lng();
        placeObj['address'] = place.formatted_address;
        placeObj['url'] = place.url;
        
        console.log(placeObj, place);
    }
    
    function geolocate() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
                var geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                var circle = new google.maps.Circle({
                    center: geolocation,
                    radius: position.coords.accuracy
                });
                autocomplete.setBounds(circle.getBounds());
            });
        }
    }
    
    function submitGeolocationForm(){
        var valid = true;
		
		jQuery('.wpcf7-validates-as-required').each(function(){
			if(jQuery(this).val() == '' ){
				valid = false;
				jQuery(this).parent().append('<span class="error">* Este campo não pode ficar em branco</span>');
			}
		})


        if(placeObj['latitude'] == '' || placeObj['longitude'] == ''){
            valid = false;
            if(jQuery('[name="flocal"]').parent().children('span.error').length == 0)
                jQuery('[name="flocal"]').parent().append('<span class="error">* Certifique-se de que selecionou um endereço válido</span>');
        }

		
		if(!valid) {
            jQuery('.wpcf7-submit').parent().preppend('<span style="margin-bottom: 30px;" class="error">* Parece que existem erros no cadastro. </span>');
            return;
        }
        
        var dados_envio = {
            'mais_noticias_nonce': js_global.mais_noticias_nonce,
            'place': placeObj,
            'action': 'mais_noticias',
            'atividade' : jQuery('form').serializeArray()
        }
        
        jQuery('.wpcf7').html('<h4 class="text-align-center">Um minuto... estamos processando sua inscrição</h4>');
        
        jQuery.ajax({
            url: js_global.xhr_url,
            type: 'POST',
            data: dados_envio,
            dataType: 'JSON',
            success: function(response) {
                jQuery('.wpcf7').html('<h3 class="text-align-center">Obrigado! Sua inscrição foi submetida com sucesso!</h3>');
            }
        });
    }
    </script>
    
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDD6lYTKhVN9Q_S6mE0jKTdCdzuYp1mROA&libraries=places&callback=initAutocomplete">
    </script>
    <style>
    .error{
        top: 30px;
        position: relative;
        color: red;
        font-size: 12px;
    }
    
    .step-counter{
        display: flex;
        width: 100%;
        height: 7px;
        position: absolute;
        bottom: 0;
    }
    
    .step-counter .step{
        min-width: 33%;
        background: #dedede;
        transition: all 1s;
        margin-right: 15px;
    }
    
    .step-counter .step.active {
        background: #6ba2d0;
    }
    
    .step-counter .counter{
        position: absolute;
        background: #769fd3;
        color: white;
        font-size: 12px;
        border-radius: 5px;
        padding: 0px 10px;
        top: 15px;
        left: 12.5%;
        transition: all 1s;
        box-shadow: 0 3px 15px #0000002e;
    }
    
    .step-counter .counter:before{
        content: '';
        width: 0;
        height: 0;
        border-left: 5px solid transparent;
        border-right: 5px solid transparent;
        border-bottom: 5px solid #7f9bd6;
        top: -5px;
        position: absolute;
        left: calc(50% - 5px);
    }
    </style>